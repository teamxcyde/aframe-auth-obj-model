# Aframe Auth Obj Model

Gets .obj and .mtl from behind jwt auth and doesn't load textures

## Usage

```html
<a-entity auth-obj-model="obj: {objUrl}; mtl: {objUrl}; token: {token}"></a-entity>
```

If using with angular, import it in the `polyfills.ts`
