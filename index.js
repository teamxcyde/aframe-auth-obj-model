if (typeof AFRAME === 'undefined') {
  throw new Error('Component attempted to register before AFRAME was available.');
}

AFRAME.registerComponent('auth-obj-model', {
  schema: {
    mtl: { type: 'string' },
    obj: { type: 'string' },
    textures: { type: 'string' },
    token: { type: 'string' },
  },

  init: function() {
    THREE.Cache.enabled = true;

    this.model = null;
    this.objLoader = new THREE.AuthOBJLoader();
    this.mtlLoader = new THREE.AuthMTLLoader(this.objLoader.manager);
    // Allow cross-origin images to be loaded.
    this.mtlLoader.crossOrigin = '';
  },

  update: function() {
    var data = this.data;

    this.objLoader.setToken(data.token);
    this.mtlLoader.setToken(data.token);
    this.mtlLoader.setTextureLoading(data.textures.length > 0);

    if (!data.obj) {
      return;
    }
    this.resetMesh();
    this.loadObj(data.obj, data.mtl, data.textures);
  },

  abort: function() {
    if (this.objLoader && this.objLoader.request && typeof this.objLoader.request.abort === 'function') this.objLoader.request.abort();
    if (this.mtlLoader && this.mtlLoader.request && typeof this.mtlLoader.request.abort === 'function') this.mtlLoader.request.abort();
  },

  remove: function() {
    if (!this.model) {
      return;
    }
    this.resetMesh();
  },

  resetMesh: function() {
    this.el.removeObject3D('mesh');
  },

  loadObj: function(objUrl, mtlUrl, textureUrl) {
    var self = this;
    var el = this.el;
    var mtlLoader = this.mtlLoader;
    var objLoader = this.objLoader;

    if (mtlUrl) {
      // .OBJ with an .MTL.
      if (el.hasAttribute('material')) {
        warn('Material component properties are ignored when a .MTL is provided');
      }
      mtlLoader.setTexturePath(textureUrl || mtlUrl.substr(0, mtlUrl.lastIndexOf('/') + 1));
      mtlLoader.load(mtlUrl, function(materials) {
        materials.preload();
        objLoader.setMaterials(materials);
        objLoader.load(objUrl, function(objModel) {
          self.model = objModel;
          el.setObject3D('mesh', objModel);
          el.emit('model-loaded', { format: 'obj', model: objModel });
        });
      });
      return;
    }

    // .OBJ only.
    objLoader.load(objUrl, function loadObjOnly(objModel) {
      // Apply material.
      var material = el.components.material;
      if (material) {
        objModel.traverse(function(child) {
          if (child instanceof THREE.Mesh) {
            child.material = material.material;
          }
        });
      }

      self.model = objModel;
      el.setObject3D('mesh', objModel);
      el.emit('model-loaded', { format: 'obj', model: objModel });
    });
  },
});
